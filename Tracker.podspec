Pod::Spec.new do |s|

  s.name         = "Tracker"
  s.version      = "0.1.0"
  s.summary      = "."

  s.description  = <<-DESC
                   ...
                   DESC

  s.homepage     = "https://bitbucket.org/adam_cichy/analyticstracker"
  s.license      = { :type => 'Commercial', :text => <<-LICENSE
    All rights reserved.
    LICENSE
  }
  s.author       = { "Adam Cichy" => "fzfpff@gmail.com" }


  s.platform     = :ios, "7.0"
  s.source       = { :git => "ssh://git@bitbucket.org/adam_cichy/analyticstracker.git", :tag => "0.1.0" }

  s.source_files = "Classes", "Tracker/**/*.{h,m}"

  s.requires_arc = true

end