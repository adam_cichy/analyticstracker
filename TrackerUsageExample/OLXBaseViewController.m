//
//  OLXBaseViewController.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXBaseViewController.h"
#import "OLXTracker.h"
#import "OLXTag.h"
#import "OLXScreenViewTag.h"

@implementation OLXBaseViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [OLXTracker logScreenView:[self olxScreenName]];
}

- (NSString *)olxScreenName { //can be overriden in subclasses
    return NSStringFromClass([self class]);
}

@end
