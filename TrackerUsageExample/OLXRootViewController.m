//
//  OLXRootViewController.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXRootViewController.h"
#import "OLXTracker.h"

@implementation OLXRootViewController

- (IBAction)btnSendEventTapped:(id)sender {
    [OLXTracker logEvent:@"MIKE_TYSON"];
}

@end
