//
//  OLXScreenViewTag.h
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXTag.h"

@interface OLXScreenViewTag : OLXTag

@property (nonatomic, copy) NSString *screenName;

@end
