//
//  OLXScreenViewTag.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXScreenViewTag.h"

@implementation OLXScreenViewTag

- (NSString *)name {
    return @"Screen view"; //a bit dirty, refactor
}

- (NSDictionary *)toDictionary {
    NSMutableDictionary *dict = [[super toDictionary] mutableCopy];
    dict[@"SCREEN_NAME"] = [self screenName];
    return dict;
}

@end
