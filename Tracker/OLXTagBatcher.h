//
//  OLXTagBatcher.h
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OLXTag.h"

typedef void(^OLXTagBatcherCompletionBlock)(NSArray *tags);

@interface OLXTagBatcher : NSObject

+ (instancetype)sharedInstance;

- (void)registerTag:(OLXTag *)olxTag;
- (void)getTagsToSend:(OLXTagBatcherCompletionBlock)completion;
- (void)removeTag:(NSUUID *)uuid;

@end
