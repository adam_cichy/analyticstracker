//
//  OLXTag.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXTag.h"

@implementation OLXTag

- (instancetype)init {
    self = [super init];
    if (self) {
        _uuid = [[NSUUID alloc]init];
    }
    return self;
}

- (NSDictionary *)toDictionary {
    return @{@"NAME": [self name]};
}

@end
