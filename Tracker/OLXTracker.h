//
//  OLXTracker.h
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OLXTag.h"

@interface OLXTracker : NSObject

+ (void)logEvent:(NSString *)eventName;
+ (void)logEvent:(NSString *)eventName params:(NSDictionary *)params;
+ (void)logTag:(OLXTag *)tag;
+ (void)logScreenView:(NSString *)screenName;

@end
