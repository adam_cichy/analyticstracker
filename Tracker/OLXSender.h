//
//  OLXSender.h
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OLXSender : NSObject

- (void)send;

@end
