//
//  OLXTracker.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXTracker.h"
#import "OLXScreenViewTag.h"
#import "OLXTagBatcher.h"
#import "OLXEventTag.h"

@implementation OLXTracker

+ (void)logEvent:(NSString *)eventName params:(NSDictionary *)params {
    NSLog(@"%@", eventName);
    OLXEventTag *tag = [OLXEventTag new];
    tag.params = params;
    tag.name = eventName;
    [self logTag:tag];
}

+ (void)logEvent:(NSString *)eventName {
    [self logEvent:eventName params:nil];
}

+ (void)logTag:(OLXTag *)tag {
    NSLog(@"%@", [tag toDictionary]);
    [[OLXTagBatcher sharedInstance] registerTag:tag];
}

+ (void)logScreenView:(NSString *)screenName {
    OLXScreenViewTag *tag = [OLXScreenViewTag new];
    tag.screenName = screenName;
    [self logTag:tag];
}

@end
