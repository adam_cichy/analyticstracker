//
//  OLXTimer.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXTimer.h"
#import "OLXSender.h"

@implementation OLXTimer {
    NSTimer *timer;
    OLXSender *sender;
}

+ (instancetype)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t onceTokenSharedInstance;
    dispatch_once(&onceTokenSharedInstance, ^{
        sharedInstance = [[self alloc]init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(tick) userInfo:nil repeats:YES];
        sender = [OLXSender new];
    }
    return self;
}

- (void)tick {
    [self doWork];
}

- (void)doWork {
//    make sender get data from batcher and try to send the data
    [sender send];
}

@end
