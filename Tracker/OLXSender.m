//
//  OLXSender.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXSender.h"
#import "OLXTagBatcher.h"
@implementation OLXSender {
    NSURLSession *session;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSString *username = @"pi3243fa"; //TODO: need this
        NSString *password = @"da39a3ee5e6b4b0d3255bfef95601890afd80709";
        NSString *authString = [NSString stringWithFormat:@"%@:%@",
                                username,
                                password];
        NSData *authData = [authString dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authHeader = [NSString stringWithFormat: @"Basic %@",
                                [authData base64EncodedStringWithOptions:0]];
        NSURLSessionConfiguration *sessionConfig =
        [NSURLSessionConfiguration defaultSessionConfiguration];
        [sessionConfig setHTTPAdditionalHeaders:@{
                                                  @"Accept": @"application/json",
                                                  @"Authorization": authHeader
                                                  }
         ];
        session = [NSURLSession sessionWithConfiguration:sessionConfig];
    }
    return self;
}

- (void)send {
    [[OLXTagBatcher sharedInstance] getTagsToSend:^(NSArray *tags) {
        for (OLXTag *tag in tags) {
            NSURL *url = [NSURL URLWithString:@"https://api.intercom.io/tags"];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
            request.HTTPMethod = @"POST";
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[tag toDictionary] options:NSJSONWritingPrettyPrinted error:nil];
            request.HTTPBody = jsonData;
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                BOOL noError = error == nil;
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                BOOL statuscodeOK = (httpResponse.statusCode / 100) == 2;
                if (noError && statuscodeOK) {
                    [[OLXTagBatcher sharedInstance] removeTag:tag.uuid];
                }
            }];
            [task resume];
        }
    }];
}

@end
