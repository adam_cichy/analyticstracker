//
//  OLXTimer.h
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OLXSender.h"

@interface OLXTimer : NSObject

+ (instancetype)sharedInstance;

@end
