//
//  OLXEventTag.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXEventTag.h"
@import UIKit;
#import <AdSupport/ASIdentifierManager.h>

@implementation OLXEventTag

- (instancetype)init
{
    self = [super init];
    if (self) {
        _idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        _deviceName = [[UIDevice currentDevice] name];
        _deviceLanguage = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
        _randomNumber = [NSString stringWithFormat:@"%d", arc4random_uniform(10000000)];
    }
    return self;
}

- (NSDictionary *)toDictionary {
    NSMutableDictionary *dict = [[super toDictionary] mutableCopy];
    dict[@"DEVICE_NAME"] = [self deviceName];
    dict[@"DEVICE_LANG"] = [self deviceLanguage];
    dict[@"DEVICE_IDFA"] = [self idfa];
    dict[@"RANDOM_NO"] = [self randomNumber];
    [dict addEntriesFromDictionary:_params];
    return dict;
}


@end
