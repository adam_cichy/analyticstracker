//
//  OLXEventTag.h
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXTag.h"

@interface OLXEventTag : OLXTag // a sample tag  impementation with other stuff

@property (nonatomic, copy) NSString *idfa;
@property (nonatomic, copy) NSString *deviceName;
@property (nonatomic, copy) NSString *deviceLanguage;
@property (nonatomic, copy) NSString *randomNumber;

@property (nonatomic, copy) NSDictionary *params;

@end
