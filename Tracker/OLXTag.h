//
//  OLXTag.h
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OLXTag : NSObject //this could also be a protocol

@property (nonatomic, copy) NSString *name;
@property (readonly) NSUUID *uuid;

- (NSDictionary *)toDictionary;

@end
