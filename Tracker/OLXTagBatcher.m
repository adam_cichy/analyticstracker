//
//  OLXTagBatcher.m
//  Tracker
//
//  Created by Adam Cichy on 02/03/16.
//  Copyright © 2016 OLX. All rights reserved.
//

#import "OLXTagBatcher.h"
#import "OLXTimer.h"

@implementation OLXTagBatcher {
    NSMutableArray *tags;
    NSMutableArray *tagsToSend;
    dispatch_queue_t processingQueue;
}

+ (instancetype)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t onceTokenSharedInstance;
    dispatch_once(&onceTokenSharedInstance, ^{
        sharedInstance = [[self alloc]init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        tags = [NSMutableArray new];
        tagsToSend = [NSMutableArray new];
        processingQueue = dispatch_queue_create("com.olx.Tracker.processingQueue", DISPATCH_QUEUE_SERIAL);
        [OLXTimer sharedInstance];//initialize singleton
    }
    return self;
}

- (void)registerTag:(OLXTag *)olxTag {
    dispatch_async(processingQueue, ^{
        [tags addObject:olxTag];
    });
}

- (void)getTagsToSend:(OLXTagBatcherCompletionBlock)completion {
    dispatch_async(processingQueue, ^{
        [tagsToSend addObjectsFromArray:tags];
        [tags removeAllObjects];
        if (completion) {
            completion(tagsToSend);
        }
    });
}

- (void)removeTag:(NSUUID *)uuid {
    dispatch_async(processingQueue, ^{
        NSUInteger tagToRemove = NSNotFound;
        for (NSUInteger i = 0; i < [tagsToSend count]; i++) {//not liking this very much
            OLXTag *tag = tagsToSend[i];
            if ([tag.uuid isEqual:uuid]) {
                tagToRemove = i;
                break;
            }
        }
        if (tagToRemove != NSNotFound) {
            [tagsToSend removeObjectAtIndex:tagToRemove];
        }
    });
}

@end
